const mongoose = require('mongoose');

let RoomSchema = new mongoose.Schema({
	name: { type: String, index: true },
    owner: String,
    location: String,
    private: { type:Boolean, default:false },
	createdOn: { type: Date, default: Date.now }
});

module.exports = RoomSchema; 