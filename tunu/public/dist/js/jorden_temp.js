document.getElementById("btnBurger").addEventListener("click", function() {
    document.getElementById("navigationDrawer").className =
      "layout__drawer--active";
  });
  
  //Close NavigationDrawer
  document.addEventListener("mouseup", function(e) {
    var container = document.getElementById("navigationDrawer");
  
    if (this != container) {
      document.getElementById("navigationDrawer").className = "layout__drawer";
    }
  });
  
//TODO de error hieruit halen die zich voort doet bij scrollen
//header colour changer while scrolling
window.addEventListener("scroll", function() {
  if (document.getElementsByTagName("html")[0].scrollTop > 300) {
    document.getElementById("nav").className = "nav--scroll";
  } else {
    document.getElementById("nav").className = document
      .getElementById("nav")
      .className.replace("nav--scroll", "");
  }
});

//Open popup
var btnLogin = document.getElementsByClassName("nav__item--login");

var popUpLoginFunction = function() {
  document.getElementById("popup-wrapper").className = "popup";
};

for (var i = 0; i < btnLogin.length; i++) {
  btnLogin[i].addEventListener("click", popUpLoginFunction, false);
}

//Close popup
document
  .getElementById("popup__btnClose")
  .addEventListener("click", function() {
    document.getElementById("popup-wrapper").className = "popup__hide";
  });

///GOOGLE Button
function onSuccess(googleUser) {
  console.log("Logged in as: " + googleUser.getBasicProfile().getName());
}
function onFailure(error) {
  console.log(error);
}
function renderButton() {
  gapi.signin2.render("my-signin2", {
    scope: "profile email",
    width: 240,
    height: 50,
    longtitle: true,
    theme: "dark",
    onsuccess: onSuccess,
    onfailure: onFailure
  });
}
