let Room = require("../../data/models/room");

module.exports = (req, res, next) => {
    Room.findOne({ _id: req.params._id }, function (err, room) {
        if (err) { return next(err) }
        if (!room) {
            return res.send('Not Found', 404);
        }
        
        req.room = room;
        next();
    });
}