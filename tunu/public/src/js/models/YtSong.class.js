export default class YtSong{

  constructor(id, thumbnail, title, published_at){

    this.id = id;
    this.thumbnail = thumbnail;
    this.title = title;
    this.published_at = published_at;
  }

}
