const webpack = require('webpack'),
	path = require('path'),
	cleanWebpackPlugin = require('clean-webpack-plugin'),
	extractPlugin = require('extract-text-webpack-plugin');

module.exports = {
	entry: ['./src/js/app.js', './src/scss/style.scss'],
	output: {
		path: path.resolve(__dirname, 'dist/js'),
		filename: 'app.bundle.js'
	},
	plugins: [
		new cleanWebpackPlugin('[dist/js]'),
		new extractPlugin({
			filename: '../css/style.css'
		})
	],
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['env']
					}
				}
			},
			{
				test: /\.scss$/,
				loader: extractPlugin.extract(['css-loader?url=false', 'sass-loader'])
			}
		]
	}
};
