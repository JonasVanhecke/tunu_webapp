const mongoose = require('mongoose');
let RoomSchema = require('../schemas/room');
let Room = mongoose.model('Room', RoomSchema, 'rooms'); //model,schema,collection

module.exports = Room;
