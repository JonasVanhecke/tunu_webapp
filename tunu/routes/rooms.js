var express = require("express");
var router = express.Router();
let Room = require("../data/models/roomsRepo");
let loadRoom = require("./middleware/loadRoom");

//
// GET ROOMS
//
router.get("/", function(req, res) {
  Room.getAllRooms((error, docs) => {
    if (error) {
      console.log(error); //handleError
    }

    res.render("rooms/index", { title: "Rooms overzicht", roomlist: docs });
  });
});

//
// CREATE ROOM
//
router.get("/new", function(req, res) {
  res.render("rooms/new", { title: "New" });
});

router.post("/new", function(req, res) {
  Room.addRoom(req, error => {
    if (error) {
      console.log(error);
    }

    res.redirect("/rooms");
  });
});

//
// DETAILS
//
router.get("/:_id", loadRoom, (req, res, next) => {
  res.render("rooms/details", { title: "Room details", room: req.room });
});

//
// EDIT
//
router.get("/edit/:_id", loadRoom, (req, res, next) => {
  res.render("rooms/edit", { title: "Room Edit", room: req.room });
});

router.post("/edit/:_id", (req, res, next) => {

  switch (req.body._method) {

	case "Delete":
	
	  Room.removeRoom(req.body._id, error => {
		if (error) {
		  console.log(error);
		}
	  });

	  break;
	  
    case "Update":

	  Room.updateRoom(req.body._id, req, error => {
		if (error) {
			console.log(error);
		  }
	  });

	  break;
	  
    default:
	  next();
	  
  }

  res.redirect("/rooms");
});

module.exports = router;
