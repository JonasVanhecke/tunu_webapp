//TODO --> fetch url
export function fetchAuth(url){

  //https://www.googleapis.com/youtube/v3/search?key=AIzaSyCF6QyTC5gnCzQRiHpq2c27Ug9SOXC_iFQ&part=snippet,id&order=date&maxResults=20&q=cats

  var myInit = { method: 'GET'};

  if(!url) throw new UrlNotFoundException();

    var p = new Promise((ok, nok) => {

    fetch(url, myInit).then(function(response){
      if(response.ok) {
        return response;
      }
      throw new Error("Network response was not ok. ");
    }).then(function(response){
      return response.json();

    }).then(function(data){
       ok(data);

    }).catch(function(err){
      nok(err);

    });

  });

  return p;
}
