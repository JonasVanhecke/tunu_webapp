import * as auth from './services/yt.service';
import YtSong from './models/YtSong.class';

var searchButton, filter, songsList;

function SearchVideos(){

  var q = filter.value;
  var apiKey = "AIzaSyCF6QyTC5gnCzQRiHpq2c27Ug9SOXC_iFQ"
  var url = `https://www.googleapis.com/youtube/v3/search?key=${apiKey}&part=snippet,id&order=rating&maxResults=20&q=${q}`;

  auth.fetchAuth(url).then(showSong, showErr);
}

function showSong(response){
  var bobHTMLBuilder = "",
    l = response.items.length;

  for(var i = 0; i < l; i++){
    var r = response.items[i];
    var nSong = new YtSong(r.id.videoId, r.snippet.thumbnails.medium.url, r.snippet.title, r.snippet.publishedAt);

    bobHTMLBuilder += `<div class="songs-list__media">`;
    bobHTMLBuilder += `<p>${nSong.title}</p>`;
    bobHTMLBuilder += `<image src="${nSong.thumbnail}"/>`;
    bobHTMLBuilder += `</div>`;

    songsList.innerHTML = bobHTMLBuilder;
  }
}

function showErr(){
  console.error(err);
}

function addEventListeners(){
  searchButton.addEventListener("click" , SearchVideos);
}

function initSocket(){

  var messages = [];
  var socket = io.connect(window.location.host, {transports: ['websocket']});
  //var field = document.getElementById("field");
  //var sendButton = document.getElementById("send");
  //var content = document.getElementById("content");

  socket.on('message', function (data) {
      if(data) {
          // messages.push(data.message);
          // var html = '';
          // for(var i=0; i<messages.length; i++) {
          //     html += messages[i] + '<br />';
          // }
          // content.innerHTML = html;
          console.log(data);
      } else {
          console.log("There is a problem:", data);
      }
  });
}

function init(){

  searchButton = document.getElementById("search-button");
  filter = document.getElementById("query");
  songsList = document.getElementById("search-container");

  initSocket();
  addEventListeners();
}

init();
