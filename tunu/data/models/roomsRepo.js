//roomsRepo
const mongoose = require('mongoose');

module.exports = (function() {

	let Room = require('./room.js');

	let getAllRooms = function(callback) {
		Room.find({}).sort('name').exec(function(err, docs) {
			if (err) {
				console.log(err);
				callback(err, null);
			}
			callback(null, docs);
		});
	};
	
	let addRoom = function(req, callback) {
		var newRoom = new Room(req.body);
		
		newRoom.save(function(err) {
			if(err) {
				console.log(err);
				callback(err, null);
			}

			callback(null);
		});
	};

	let removeRoom = function(id, callback) {
		Room.remove({ _id : id }, function(err) {
			if(err) {
				console.log(err);
				callback(err, null);
			}

			callback(null);
		});
	};

	let updateRoom = function(id, req, callback) {
		Room.findOneAndUpdate(id, req.body, { upsert:true }, function(err) {
			if(err) {
				console.log(err);
				callback(err, null);
			}

			callback(null);
		});
	};

	return {
		Room,
		getAllRooms,
		addRoom,
		removeRoom,
		updateRoom
	};
})();
