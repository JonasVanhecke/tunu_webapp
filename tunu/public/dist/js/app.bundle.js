/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(4);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _yt = __webpack_require__(2);

var auth = _interopRequireWildcard(_yt);

var _YtSong = __webpack_require__(3);

var _YtSong2 = _interopRequireDefault(_YtSong);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var searchButton, filter, songsList;

function SearchVideos() {

  var q = filter.value;
  var apiKey = "AIzaSyCF6QyTC5gnCzQRiHpq2c27Ug9SOXC_iFQ";
  var url = 'https://www.googleapis.com/youtube/v3/search?key=' + apiKey + '&part=snippet,id&order=rating&maxResults=20&q=' + q;

  auth.fetchAuth(url).then(showSong, showErr);
}

function showSong(response) {
  var bobHTMLBuilder = "",
      l = response.items.length;

  for (var i = 0; i < l; i++) {
    var r = response.items[i];
    var nSong = new _YtSong2.default(r.id.videoId, r.snippet.thumbnails.medium.url, r.snippet.title, r.snippet.publishedAt);

    bobHTMLBuilder += '<div class="songs-list__media">';
    bobHTMLBuilder += '<p>' + nSong.title + '</p>';
    bobHTMLBuilder += '<image src="' + nSong.thumbnail + '"/>';
    bobHTMLBuilder += '</div>';

    songsList.innerHTML = bobHTMLBuilder;
  }
}

function showErr() {
  console.error(err);
}

function addEventListeners() {
  searchButton.addEventListener("click", SearchVideos);
}

function initSocket() {

  var messages = [];
  var socket = io.connect(window.location.host, { transports: ['websocket'] });
  //var field = document.getElementById("field");
  //var sendButton = document.getElementById("send");
  //var content = document.getElementById("content");

  socket.on('message', function (data) {
    if (data) {
      // messages.push(data.message);
      // var html = '';
      // for(var i=0; i<messages.length; i++) {
      //     html += messages[i] + '<br />';
      // }
      // content.innerHTML = html;
      console.log(data);
    } else {
      console.log("There is a problem:", data);
    }
  });
}

function init() {

  searchButton = document.getElementById("search-button");
  filter = document.getElementById("query");
  songsList = document.getElementById("search-container");

  initSocket();
  addEventListeners();
}

init();

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fetchAuth = fetchAuth;
//TODO --> fetch url
function fetchAuth(url) {

  //https://www.googleapis.com/youtube/v3/search?key=AIzaSyCF6QyTC5gnCzQRiHpq2c27Ug9SOXC_iFQ&part=snippet,id&order=date&maxResults=20&q=cats

  var myInit = { method: 'GET' };

  if (!url) throw new UrlNotFoundException();

  var p = new Promise(function (ok, nok) {

    fetch(url, myInit).then(function (response) {
      if (response.ok) {
        return response;
      }
      throw new Error("Network response was not ok. ");
    }).then(function (response) {
      return response.json();
    }).then(function (data) {
      ok(data);
    }).catch(function (err) {
      nok(err);
    });
  });

  return p;
}

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var YtSong = function YtSong(id, thumbnail, title, published_at) {
  _classCallCheck(this, YtSong);

  this.id = id;
  this.thumbnail = thumbnail;
  this.title = title;
  this.published_at = published_at;
};

exports.default = YtSong;

/***/ }),
/* 4 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);